.. unit-harvest documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Unit harvest
============

The `unit-harvest <https://gitlab.com/cfinan/unit-harvest>`_ package contains some scripts to gather reference ranges and units of common biomarkers used in clinical practice.

Contents
========

.. toctree::
   :maxdepth: 2
   :caption: Setup

   getting_started

.. toctree::
   :maxdepth: 2
   :caption: Programmer reference

   scripts
   examples

.. toctree::
   :maxdepth: 2
   :caption: Project admin

   contributing

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
