# Unit harvest

__version__: `0.1.1a0`

Scrape reference ranges for lab tests from various websites.

There is [online](https://cfinan.gitlab.io/unit-harvest/index.html) documentation for gwas-catalog-parser.

## Installation instructions
At present, unit-harvest is undergoing development and no packages exist yet on PyPi. Therefore it is recommended that you install in either of the two ways listed below.

### Installation using conda
I maintain a conda package in my personal conda channel. To install from this please run:

```
conda install -c cfin -c bioconda -c conda-forge unit-harvest
```

There are currently builds for Python v3.8, v3.9 and v3.10 for Linux-64 and Mac-osx. Please keep in mind that all development is carried out on Linux-64 and Python v3.8/v3.9. I do not own a Mac so can't test on one, the conda build does run some import tests but that is it.

### Installation using pip
You can install using pip from the root of the cloned repository, first clone and cd into the repository root:

```
git clone git@gitlab.com:cfinan/unit-harvest.git
cd unit-harvest
```

Install the dependencies:
```
python -m pip install --upgrade -r requirements.txt
```

Then install using pip
```
python -m pip install .
```

Or for an editable (developer) install run the command below from the root of the repository. The difference with this is that you can just to a `git pull` to update, or switch branches without re-installing:
```
python -m pip install -e .
```

### Conda dependencies
There are also conda yaml environment files in `./resources/conda/envs` that have the same contents as `requirements.txt` but for conda packages, so all the pre-requisites. I use this to install all the requirements via conda and then install the package as an editable pip install.

However, if you find these useful then please use them. There are Conda environments for Python v3.8, v3.9 and v3.10.

### Installing webdrivers
Currently Selanium and [Google Chrome](https://www.google.co.uk/chrome/) is used in headless or browser mode. So chrome (or chromium if you prefer) and the corresponding [webdriver](https://chromedriver.chromium.org/downloads) must be available. The webdriver version must be matched to your chrome/chromium version. If the webdriver is not in your PATH it should be specified on the command line.
