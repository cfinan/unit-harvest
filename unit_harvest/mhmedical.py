"""Extract reference range data from the reference ranges table in the book:
Laboratory Medicine: The diagnosis of disease in the Clinical laboratory.

This is from the table in the `book <http://accessmedicine.mhmedical.com/content.aspx?bookid=1069&sectionid=60775149>`_

Please note, that this matches section headings in the table with tests based
 on indentation. This is not always uniform so some manual hard set rules are
 also used. These will be fine unless there are changes, it is just something
 to be aware of.
"""
from unit_harvest import (
    __name__ as pkg_name,
    __version__,
    common
)
from selenium.common.exceptions import NoSuchElementException
from pyaddons import log, utils
from tqdm import tqdm
import argparse
import csv
import os
import re
import warnings
# import pprint as pp


# Move a load of these to constants
# The prefix for verbose messages and the location to write the messages
HOME = (
    "http://accessmedicine.mhmedical.com/content.aspx?bookid=1069&"
    "sectionid=60775149"
)
"""The webpage URL containing the table of reference range data (`str`)
"""
_PROG_NAME = 'units-scrape-mh-medical'
"""The name of the program (`str`)
"""
_DESC = __doc__
"""The program description (`str`).
"""

IS_SECTION_HEADER = "is_section_header"
"""The tag name for a section header (`str`).
"""
IS_HANGING_ROW = "is_hanging_row"
"""The tag name for a hanging row indicator (`str`).
"""
ENZYME_KATAL_UNITS = "enzyme_katal_units"
"""The tag name for a enzyme katal indicator (`str`).
"""
HAS_AGE_RANGE = "has_age_range"
"""The tag name for a has age range indicator (`str`).
"""
AGE_GROUP = "age_group"
"""The tag name for a age group (`str`).
"""
TEST = "test"
"""The tag name for the test (`str`).
"""
RANGE_TYPE = "range_type"
"""The tag name for the range type (`str`).
"""
SEX = 'sex'
"""The tag name for the sex (`str`).
"""
SEX_CHARACTER = 'sex_characteristic'
"""The tag name for the sex characteristic (`str`).
"""
INDENT_LEVEL = 'indent_level'
"""The tag name for the indent level (`str`).
"""
SYNONYMS = 'synonyms'
"""The tag name for any synonyms (`str`).
"""
ROW = 'row'
"""The tag name for the row (`str`).
"""
SECTION = 'section'
"""The tag name for any sections (`str`).
"""

MANUAL_SECTIONS = {
    'Cholesterol, low-density lipoproteins (LDL)': 5,
    'C-telopeptide': 2,
    'γ-Glutamyltransferase (GGT; γ-glutamyl transpeptidase)': 2,
    'Glycated hemoglobin (hemoglobin A1, A1c)': 2,
    'Inhibin A': 6,
    'Mexthotrexate': 3,
    'N-telopeptide (BCE, bone collagen equivalents)': 2,
    'Pyridinium cross-links (deoxypyridinoline)': 2,
}
"""Manual sections to correct for formatting errors in the table. The keys are
 section names and the values are number or additional rows to which the
 section applies (`dict`)
"""

_SYNONYM_REGEXP = re.compile(r'\(see (.+)\)$')
"""A regular expression to match synonyms (`re.Pattern`).
"""

_OUTHEADER = [
    'test',
    'conditional',
    'has_age_range',
    'age_group',
    'enzyme_katal_units',
    'synonyms',
]
"""The output file header (`list` of `str`).
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    logger = log.init_logger(_PROG_NAME, verbose=args.verbose)
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    webdriver = common.initialise_chrome(
        driver_path=args.webdriver,
        download_dir=args.outdir,
        headless=not args.browser
    )

    outfile = os.path.join(args.outdir, 'mh-medical-refrange.txt')
    prog_verbose = log.progress_verbose(verbose=args.verbose)
    get_table_content(webdriver, outfile, verbose=prog_verbose)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parsed arguments.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object.
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )

    # The wide format UKBB data fields filex
    parser.add_argument(
        'outdir', type=str,
        help="The output directory to write the finished files"
    )
    parser.add_argument(
        '--webdriver', type=str, help="The path to the chrome-webdriver"
    )
    parser.add_argument(
        '-v', '--verbose',  action="count",
        help="give more output"
    )
    parser.add_argument(
        '-b', '--browser',  action="store_true",
        help="The default is to use headless, if you have issues with "
        "headless then use this to turn the browser window on"
    )
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line argsand expand any relative paths.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The parser with the command line options set.

    Returns
    -------
    args : `argparse.Namespace`
        The parsed command line options
    """
    args = parser.parse_args()

    for i in ['outdir', 'webdriver']:
        try:
            setattr(args, i, os.path.expanduser(getattr(args, i)))
        except TypeError:
            # It will probably be NoneType (the system tmp location
            pass

    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def go_home(webdriver):
    """Navigate to the home directory.

    Parameters
    ----------
    webdriver : `webdriver.Chrome`
        The selenium webdriver.
    """
    webdriver.get(HOME)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_table_content(webdriver, outfile, verbose=False):
    """Extract the contents of the table.

    Parameters
    ----------
    webdriver : `webdriver.Chrome`
        The selenium webdriver.
    outfile : `str`
        The output file name, if it has a .gz extension then a gzip compressed
        file will be output.
    verbose : `bool`, optional, default: `False`
        Turn on progress monitoring.
    """
    tqdm_kwargs = dict(
        desc="[info] processing",
        unit=" rows",
        disable=not verbose
    )

    # Get the open method, a gz extension will give gzipped files
    open_method = utils.get_open_method(outfile)

    with open_method(outfile, 'wt') as outcsv:
        gen = yield_table_rows(webdriver)
        row = next(gen)
        x = list(row[ROW].keys())
        x.pop(x.index(TEST))
        header = _OUTHEADER + x
        writer = csv.DictWriter(outcsv, header, delimiter="\t")
        writer.writeheader()
        writer.writerow(_build_row(row))
        for row in tqdm(yield_table_rows(webdriver), **tqdm_kwargs):
            writer.writerow(_build_row(row))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _build_row(row):
    """Build an output row.

    Parameters
    ----------
    row : `dict`
        A raw row to process. This is a nested ``dict``.

    Returns
    -------
    row : `dict`
        An output row. This is a flat ``dict`` for use with ``csv.DictWriter``.
    """
    test = row[TEST].strip()
    conditional = None
    has_age_range = row[HAS_AGE_RANGE]
    enzyme_katal_units = row[ENZYME_KATAL_UNITS]
    if len(row[SECTION]) > 0:
        test = '|'.join([i[TEST].strip() for i in row[SECTION]])
        conditional = row[TEST].strip()
        has_age_range = False
        enzyme_katal_units = False
        for i in row[SECTION]:
            has_age_range = has_age_range | i[HAS_AGE_RANGE]
            enzyme_katal_units = \
                enzyme_katal_units | i[ENZYME_KATAL_UNITS]
    del row[ROW][TEST]
    return {
        **dict(
            test=test,
            conditional=conditional,
            has_age_range=has_age_range,
            age_group=None,
            enzyme_katal_units=enzyme_katal_units,
            synonyms=None,
        ),
        **row[ROW]
    }


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def yield_table_rows(webdriver):
    """Yield processed rows from the table.

    Parameters
    ----------
    webdriver : `webdriver.Chrome`
        The selenium webdriver

    Yields
    ------
    row : `dict`
        A raw row. This is a nested ``dict``.
    """
    # Navigate to the table home page
    go_home(webdriver)

    # Extract the table header
    header = get_table_header(webdriver)

    synonyms = dict()
    section = []
    manual_steps = 0
    for row in webdriver.find_elements_by_xpath(
        '//div[contains(@class, "tTable")]/table/tbody/tr'
    ):
        columns = row.find_elements_by_xpath('./td')

        # Will hold the actual row data
        colmap = {}

        continue_loop = False

        # Map the columns to the header
        for idx, i in enumerate(header):
            try:
                colmap[i] = columns[idx]

                # Some cases where there are hanging rows that have all
                # the columns but with empty data
                if idx > 0 and colmap[i].text.strip() == '':
                    raise IndexError("actually a section row")
            except IndexError:
                # in table section header or synonym
                tags = process_section_row(row)
                try:
                    manual_steps = MANUAL_SECTIONS[tags[TEST]]
                except KeyError:
                    manual_steps = 0

                if len(tags[SYNONYMS]) > 0:
                    for s in synonyms:
                        synonyms[s] = tags
                else:
                    try:
                        if tags[INDENT_LEVEL] > section[-1][INDENT_LEVEL]:
                            section.append(tags)
                        elif tags[INDENT_LEVEL] == section[-1][INDENT_LEVEL]:
                            section = section[:-1]
                            section.append(tags)
                        else:
                            section = [tags]
                    except IndexError:
                        section.append(tags)
                continue_loop = True
                break
        if continue_loop is True:
            continue

        tags = get_element_text(colmap[TEST])
        tags[ROW] = dict([(k, v.text) for k, v in colmap.items()])

        if tags[INDENT_LEVEL] == 0 and manual_steps == 0:
            section = []
        elif tags[INDENT_LEVEL] == 0 and manual_steps > 0:
            tags[SECTION] = section
            manual_steps -= 1
        else:
            tags[SECTION] = section

        yield tags


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_table_header(webdriver):
    """Get the header for the clinical tests table.

    Parameters
    ----------
    webdriver : `webdriver.Chrome`
        The selenium webdriver.

    Returns
    -------
    header : `list` of `str`
        The table header row
    """
    table_header = webdriver.find_elements_by_xpath(
        '//div[contains(@class, "tTable")]/table/thead/tr/th'
    )
    header = []
    for idx, i in enumerate(table_header):
        # The first column of the table does not have any column name
        if idx == 0 and i.text == '':
            header.append('test')
            continue
        # Remove any unwanted characters and replace spaces with underscores
        text = i.text
        text = re.sub(r',.+$', '', text)
        text = text.lower().replace(' ', '_')
        header.append(text)
    return header


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_tag_dict():
    """Get an empty tag dictionary.

    Returns
    -------
    tags : `dict`
        An empty tag dictionary.
    """
    return {
        ENZYME_KATAL_UNITS: False,
        IS_SECTION_HEADER: False,
        IS_HANGING_ROW: False,
        HAS_AGE_RANGE: False,
        AGE_GROUP: None,
        TEST: None,
        RANGE_TYPE: "any",
        SEX: "any",
        SEX_CHARACTER: "NA",
        INDENT_LEVEL: 0,
        SYNONYMS: [],
        ROW: None,
        SECTION: []
    }


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def process_section_row(row):
    """Process a row that is a section row.

    Parameters
    ----------
    row : `selanium.WebElement`
        The webelement to process.

    Returns
    -------
    tags : `dict`
        The tags that are associated with the text.
    """
    tags = get_element_text(row)
    tags[IS_SECTION_HEADER] = True
    tags[SYNONYMS], tags[TEST] = get_synonyms(tags[TEST])
    return tags


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_element_text(element):
    """Extract the text from an element and process the metadata.

    This handles superscripts and hanging indent.

    Parameters
    ----------
    element : `selanium.WebElement`
        The webelement to process.

    Returns
    -------
    tags : `dict`
        The tag metadata extracted from the row.
    """
    text = element.text
    tags = get_tag_dict()
    try:
        superscript_text = get_superscript_text(element)
        tags = proc_superscript_text(tags, superscript_text)
        for pat, repl in [(r'{0}$'.format(re.escape(superscript_text)), ''),
                          (r',\s*{0}'.format(re.escape(superscript_text)), ''),
                          (r'{0} (\()'.format(re.escape(superscript_text)),
                           ' \\1')]:
            text, nreplace = re.subn(pat, repl, text)
            if nreplace > 0:
                break
    except NoSuchElementException:
        # no superscripts
        pass

    # Most hanging rows are indented (but not all), so we will also look for
    # other mechanisms to check
    text_len = len(text)
    text = text.lstrip()
    text_diff = text_len - len(text)

    if text_diff > 0:
        tags[IS_HANGING_ROW] = True
        tags[INDENT_LEVEL] = text_diff
    tags[TEST] = text.strip()
    return tags


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_synonyms(text):
    """Get any synonyms from the text.

    Parameters
    ----------
    text : `str`
        The text to extract synonyms from.

    Returns
    -------
    synonyms : `list` of `str`
        The synonyms for the text, if there are none, then this will be empty.
    """
    syn_match = _SYNONYM_REGEXP.search(text)

    try:
        synonyms = [i.strip() for i in syn_match.group(1).split(',')]
        text = _SYNONYM_REGEXP.sub('', text).strip()
    except AttributeError:
        synonyms = []
    return synonyms, text


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_superscript_text(element):
    """Get superscript text from a selenium.WebElement.

    Parameters
    ----------
    element : `selenium.WebElement`
        The web element to attempt to extract superscript text from.

    Returns
    -------
    superscript_text : `str`
        The superscript text.
    """
    superscript = element.find_element_by_xpath('.//sup')
    return superscript.text


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def proc_superscript_text(tags, text):
    """Process the metadata from the superscript text.

    Parameters
    ----------
    tags : `dict`
        The tag dict.
    text : `str`
        The superscript text.

    Returns
    -------
    tags : `dict`
        The updated tag dict.
    """
    for i in re.split(r',\s*', text):
        if i == 'a':
            tags["enzyme_katal_units"] = True
        elif i == 'b':
            tags["has_age_range"] = True
        else:
            warnings.warn("unknown superscript '{0}'".format(text))
    return tags


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
