# Getting Started with skeleton-package

## The skeleton-package
This provides a template python package with all the various attributes implemented. You will need to change some names and references in various files. This is documented in `skeleton_package_overview.rst`. You will want to delete this paragraph for your own package but keep and modify everything below. You can install the skeleton-package with:

```
# From the root of skeleton-package
python -m pip install .
```

__version__: `0.1.0a1`

The skeleton-package package is a toolkit to......

There is [online](https://filedn.eu/lIeh2yR6LSw58g5HQMThnmR) documentation for skeleton-package and offline PDF documentation can be downloaded [here](https://gitlab.com/cfinan/skeleton-package/-/blob/master/resources/pdf/skeleton-package.pdf).

## Installation instructions
At present, skeleton-package is undergoing development and no packages exist yet on PyPy or in conda. Therefore it is recommended that it is installed in either of the two ways listed below. First, clone this repository and then `cd` to the root of the repository.

```
# Change in your package - this is not in git
git clone git@gitlab.com:cfinan/skeleton-package.git
cd skeleton-package
```

### Installation not using any conda dependencies
If you are not using conda in any way then install the dependencies via `pip` and install skeleton-package as an editable install also via pip:

Install dependencies:
```
python -m pip install --upgrade -r requirements.txt
```

For an editable (developer) install run the command below from the root of the skeleton-package repository:
```
python -m pip install -e .
```

### Installation using conda dependencies
A conda environment is provided in a `yaml` file in the directory `./conda_env`. A new conda environment called `/conda_` can be built using the command:

```
# From the root of the skeleton-package repository
conda env create --file ./resources/conda_env/conda_create.yml
```

To add to an existing environment use:
```
# From the root of the skeleton-package repository
conda env update --file ./resources/conda_env/conda_update.yml
```

For an editable (developer) install run the command below from the root of the skeleton-package repository:
```
python -m pip install -e .
```

Note that some of the dependencies do not have up-to-date conda builds so will need to be installed with `pip`, please see....

### Imported repositories
The list below represents all the repositories imported by the various modules. These are listed in `./requirements.txt` and in the `./resources/conda_env/conda_update.yml`.

* [`pytest-dependency`](https://pytest-dependency.readthedocs.io/en/stable/)

### Required repositories not in pypy or conda
All of the methods listed below will require the installation of some packages that are not yet in in the public domain. These will have to be cloned and installed as editable versions:

* [`simple-progress`](https://gitlab.com/cfinan/simple_progress)

## Next steps...
After installation you will want to:

1. Run the tests using ``pytest ./tests``

## Command endpoints
Installation will install the following endpoints. Usage of all these scripts can be listed with `<COMMAND> --help`.

### For printing words on the screen
* `skeleton-cmd-line` - A pointless command line program
