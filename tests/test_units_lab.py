"""Tests to make sure the scrape still works for mh-medical. This uses web
 queries.
"""
from unit_harvest import units_lab, common
from unit_harvest.example_data import examples
import pytest
import urllib.request
# import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def is_up(url):
    """Test to see if a webpage can be accessed.

    Parameters
    ----------
    url : `str`
        The URL to test.

    Returns
    -------
    is_up : `bool`
        True if a 200 code was received, False if not.
    """
    code = urllib.request.urlopen(url).getcode()
    return code == 200


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.dependency()
def test_internet():
    """Test that we can connect to the outside world
    """
    assert is_up('https://www.google.com/'), "resource not available"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.dependency(depends=['test_internet'])
def test_units_lab_is_up():
    """Test we can connect to units lab
    """
    assert is_up(units_lab.HOME), "resource not available"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.dependency(depends=['test_internet'])
def test_webdriver(tmpdir):
    """Test that the webdriver can be initialised, the webdriver needs to be
    in your path.
    """
    # Initialise the chrome webdriver
    webdriver = common.initialise_chrome(
        download_dir=str(tmpdir),
        headless=True
    )
    webdriver.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.dependency(depends=['test_units_lab_is_up'])
def test_n_clinical_tests(tmpdir):
    """Test that the correct number of clinical tests are returned
    """
    # Initialise the chrome webdriver
    webdriver = common.initialise_chrome(
        download_dir=str(tmpdir),
        headless=True
    )

    try:
        # Navigate to the units lab home directory
        units_lab.go_home(webdriver)
        tests = units_lab.get_test_list(webdriver)

        # I am using >= just in case any are added
        assert len(tests) >= 237, "wrong number of clinical tests"
    finally:
        webdriver.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.dependency(depends=['test_webdriver'])
def test_expected_data(tmpdir):
    """Test that the correct number of clinical tests are returned
    """
    exp_data = examples.get_data("units_lab")
    # Initialise the chrome webdriver
    webdriver = common.initialise_chrome(
        download_dir=str(tmpdir),
        headless=True
    )

    try:
        # Navigate to the units lab home directory
        units_lab.go_home(webdriver)
        tests = units_lab.get_test_list(webdriver)

        t = tests[0]
        cache_file = units_lab.get_cache_file(t)
        assert cache_file in exp_data, "expected data does not exist"
        data = units_lab.get_test_data(webdriver, t)
        data['name'] = t.name
        assert exp_data[cache_file] == data, "wrong data returned"
    finally:
        webdriver.close()
