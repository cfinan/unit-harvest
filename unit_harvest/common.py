"""Common functions that are used by many of the scripts
"""
from selenium.webdriver.chrome.options import Options
from selenium import webdriver


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def initialise_chrome(driver_path=None, download_dir=None, headless=False):
    """Initialise a chrome webdriver to download the files.

    Note that I can't make this headless at the moment as downloading does not
    seem to work in headless mode

    driver : `str`
        The path to the chrome webdriver (if it is not in your path).
    """

    chrome_options = Options()
    # Downloads do not work in headless mode - see here for potential solutions
    # https://github.com/TheBrainFamily/chimpy/issues/108
    if headless is True:
        chrome_options.add_argument("--headless")

    chrome_options.add_argument("--disable-extensions")
    chrome_options.add_argument("--disable-notifications")
    chrome_options.add_argument("--disable-gpu")

    if download_dir is not None:
        prefs = {"profile.default_content_settings.popups": 0,
                 "download.default_directory": download_dir,
                 "directory_upgrade": True}
        chrome_options.add_experimental_option("prefs", prefs)

    kwargs = {'options': chrome_options}
    if driver_path is not None:
        kwargs['executable_path'] = driver_path

    driver = webdriver.Chrome(**kwargs)

    return driver
