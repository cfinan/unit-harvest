==============
Python scripts
==============

Below is a list of all the command line endpoints installed with unit-harvest.

``units-scrape-units-lab``
--------------------------

.. argparse::
   :module: unit_harvest.units_lab
   :func: _init_cmd_args
   :prog: units-scrape-units-lab

Example usage
~~~~~~~~~~~~~

.. code-block:: console

   $ units-scrape-units-lab -vv .
   === units-scrape-units-lab (unit_harvest v0.1.0a1) ===
   [info] 07:49:36 > outdir value: /home/user/units_data
   [info] 07:49:36 > verbose value: 2
   [info] 07:49:36 > webdriver value: None
   [info] 07:49:49 > there are #tests: 237
   [info] processing: 100%|+++++++++++++++++++++| 237/237 [53:45<00:00, 13.61s/ rows]

Output file
~~~~~~~~~~~

The output file will be called ``units-lab-data.p`` and will be located in your output directory. This is a Python pickle file that will load a dictionary with the keys being the cache file names and the values being a nested dictionary. An example of a single entry is shown below.

.. code-block:: python

   >>> import pickle
   >>> import pprint as pp
   >>> with open("units-lab-data.p", 'rb') as infile:
   ...     data = pickle.load(infile)
   >>> pp.pprint(data['/home/user/.units_data/00610afba1c2969d44c74d25b5f062bc)
   {'conversions': {1: [('nkat/L', '1'),
                        ('µkat/L', '0.001'),
                        ('U/L', '0.06'),
                        ('IU/L', '0.06'),
                        ('µmol/(min•L)', '0.06'),
                        ('µmol/(h•L)', '3.6000'),
                        ('µmol/(h•mL)', '0.0036'),
                        ('nmol/(s•L)', '1'),
                        ('µmol/(s•L)', '0.001')],
                    10: [('nkat/L', '10'),
                         ('µkat/L', '0.01'),
                         ('U/L', '0.6'),
                         ('IU/L', '0.6'),
                         ('µmol/(min•L)', '0.6'),
                         ('µmol/(h•L)', '36.0000'),
                         ('µmol/(h•mL)', '0.036'),
                         ('nmol/(s•L)', '10'),
                         ('µmol/(s•L)', '0.01')],
                    25: [('nkat/L', '25'),
                         ('µkat/L', '0.025'),
                         ('U/L', '1.5'),
                         ('IU/L', '1.5'),
                         ('µmol/(min•L)', '1.5'),
                         ('µmol/(h•L)', '90.0000'),
                         ('µmol/(h•mL)', '0.09'),
                         ('nmol/(s•L)', '25'),
                         ('µmol/(s•L)', '0.025')],
                    50: [('nkat/L', '50'),
                         ('µkat/L', '0.05'),
                         ('U/L', '3'),
                         ('IU/L', '3'),
                         ('µmol/(min•L)', '3'),
                         ('µmol/(h•L)', '180.0001'),
                         ('µmol/(h•mL)', '0.18'),
                         ('nmol/(s•L)', '50'),
                         ('µmol/(s•L)', '0.05')],
                    75: [('nkat/L', '75'),
                         ('µkat/L', '0.075'),
                         ('U/L', '4.5'),
                         ('IU/L', '4.5'),
                         ('µmol/(min•L)', '4.5'),
                         ('µmol/(h•L)', '270.0001'),
                         ('µmol/(h•mL)', '0.2699'),
                         ('nmol/(s•L)', '75'),
                         ('µmol/(s•L)', '0.075')],
                    100: [('nkat/L', '100'),
                          ('µkat/L', '0.1'),
                          ('U/L', '6'),
                          ('IU/L', '6'),
                          ('µmol/(min•L)', '6'),
                          ('µmol/(h•L)', '360.0001'),
                          ('µmol/(h•mL)', '0.3599'),
                          ('nmol/(s•L)', '100'),
                          ('µmol/(s•L)', '0.1')]},
    'name': 'Creatine Kinase-MB (activity)',
    'synonyms': ['CKMB'],
    'units': ['nkat/l',
              'µkat/l',
              'nmol/(s•L)',
              'µmol/(s•L)',
              'U/L',
              'IU/L',
              'µmol/(min•L)',
              'µmol/(h•L)',
              'µmol/(h•mL)']}


``units-scrape-mh-medical``
---------------------------

.. argparse::
   :module: unit_harvest.mhmedical
   :func: _init_cmd_args
   :prog: units-scrape-mh-medical

Example usage
~~~~~~~~~~~~~

Example usage is shown below, you may get a warning that you can ignore.

.. code-block:: console

   $ units-scrape-mh-medical -vv .
   === units-scrape-mh-medical (unit_harvest v0.1.0a1) ===
   [info] 08:00:36 > browser value: False
   [info] 08:00:36 > outdir value: .
   [info] 08:00:36 > verbose value: 2
   [info] 08:00:36 > webdriver value: None
   [info] processing: 57 rows [00:23,  3.43 rows/s]/home/rmjdcfi/code/unit-harvest/unit_harvest/mhmedical.py:542: UserWarning: unknown superscript '+'
     warnings.warn("unknown superscript '{0}'".format(text))
   [info] processing: 473 rows [02:38,  2.99 rows/s]

Output file
~~~~~~~~~~~

The output file will be called ``mh-medical-refrange.txt`` and will be located in your output directory. The columns in the output file are shown below.

.. include:: ./data_dict/mh-medical.rst
