========================
Working out the webpages
========================

The code examples listed below detail, the testing used to workout how to extract the information from the various webpages containing clinical tests and reference range data. They are here for reference rather than examples that the user can run.

.. toctree::
   :maxdepth: 4

   examples/mhmedical
   examples/units_lab
