"""Scrape units from https://unitslab.com . This will extract the clinical
 test, synonyms, units used and derive conversions between units over a
 range on input units.

This will cache the downloaded data in a directory ``~/.units_data``, so if it
 is interrupted it will pick up from that point. The cache is removed upon
 successful execution. You may have to run several times to get everything.

The output file is a Python pickle file of the extracted data.
"""
from unit_harvest import (
    common, __name__ as pkg_name,
    __version__
)
from selenium.common.exceptions import NoSuchElementException
from collections import namedtuple
from pyaddons import log
from tqdm import tqdm
from hashlib import md5
import argparse
import os
import re
import pickle
import warnings
import shutil
# import pprint as pp


HOME = "http://unitslab.com"
"""The units lab home directory
"""
_PROG_NAME = 'units-scrape-units-lab'
"""The name of the program (`str`)
"""
_DESC = __doc__
"""The program description (`str`).
"""

# To hold the links to all the tests
Test = namedtuple('Test', ['name', 'url', 'element'])
"""A holder for the lab test data (`namedtuple`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script. For API access use
    `unit_harvest.units_lab.get_all_tests`
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    logger = log.init_logger(_PROG_NAME, verbose=args.verbose)
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    # Initialise the chrome webdriver
    webdriver = common.initialise_chrome(
        driver_path=args.webdriver,
        download_dir=args.outdir,
        headless=not args.browser
    )

    # Now process
    get_all_tests(webdriver, args.outdir, verbose=args.verbose)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parsed arguments.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object.
    """
    parser = argparse.ArgumentParser(description=_DESC)

    # The wide format UKBB data fields filex
    parser.add_argument(
        'outdir', type=str,
        help="The output directory to write the finished "
        "files"
    )
    parser.add_argument(
        '--webdriver', type=str,
        help="The path to the chrome-webdriver"
    )
    parser.add_argument(
        '-v', '--verbose',  action="count",
        help="Give more output, use -vv for progress monitoring"
    )
    parser.add_argument(
        '-b', '--browser',  action="store_true",
        help="The default is to use headless, if you have issues with "
        "headless then use this to turn the browser window on"
    )
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line argsand expand any relative paths

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The parser with the command line options set.

    Returns
    -------
    args : `argparse.Namespace`
        The parsed command line options.
    """
    args = parser.parse_args()

    for i in ['outdir', 'webdriver']:
        try:
            setattr(
                args, i,
                os.path.realpath(os.path.expanduser(getattr(args, i)))
            )
        except TypeError:
            # It will probably be NoneType (the system tmp location
            pass

    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_all_tests(webdriver, download_dir, verbose=False):
    """Get all the unitslab tests and perform the unit conversions.

    Parameters
    ----------
    webdriver : `webdriver.Chrome`
        The selenium webdriver.
    download_dir : `str`
        The download directory
    verbose : `bool` or `int`, optional, default: `False`
        Log process, use an `int` > 1 for progress monitoring.
    """
    logger = log.retrieve_logger(_PROG_NAME, verbose=verbose)
    prog_verbose = log.progress_verbose(verbose=verbose)

    # We will cache the downloaded data before outputting
    cache_dir = os.path.join(os.environ['HOME'], '.units_data')
    os.makedirs(cache_dir, exist_ok=True)

    # Will hold lab tests that don't scrape
    warning_tests = []

    # Navigate to the units lab home directory
    go_home(webdriver)

    # Get all the lab tests
    tests = get_test_list(webdriver)
    logger.info(f"there are #tests: {len(tests)}")

    # Now process all the lab tests
    for t in tqdm(tests, desc="[info] processing", unit=" rows",
                  disable=not prog_verbose):
        # Get the name of the cache file.
        cache_file = os.path.join(
            cache_dir,
            get_cache_file(t)
        )

        # If we have already downloaded
        if os.path.exists(cache_file):
            continue

        try:
            # Get the test data
            data = get_test_data(webdriver, t)
            with open(cache_file, 'wb') as outfile:
                pickle.dump(data, outfile)
        except ValueError:
            # Didn't work so append to warn at the end
            warning_tests.append(t)

    # Warn if needed
    if len(warning_tests) > 0:
        warnings.warn(
            "Problem getting tests, please re-run to get "
            "these: {0}".format(
                ",".join([i.name for i in warning_tests])
            )
        )
    else:
        # Output the test data

        # Merge all the pickle files into a single dictionary
        # then output
        all_tests = dict()
        for t in tests:
            cache_file = os.path.join(
                cache_dir,
                get_cache_file(t)
            )
            with open(cache_file, 'rb') as infile:
                data = pickle.load(infile)
                data['name'] = t.name
                all_tests[cache_file] = data

        outpath = os.path.join(
            download_dir, "units-lab-data.p"
        )
        with open(outpath, 'wb') as outfile:
            pickle.dump(all_tests, outfile)

        shutil.rmtree(cache_dir)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_cache_file(test):
    """Get the cache file name.

    Parameters
    ----------
    test : `unit_harvest.units_lab.Test`
        The test to generate the cache file name for.

    Returns
    -------
    cache_file_name : `str`
        The cache file name, this is an MD5 sum of a _ concatenated test name
        and the URL with a pickle file extensions.
    """
    return md5(f"{test.name}_{test.url}.p".encode()).hexdigest()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def go_home(webdriver):
    """Navigate the home directory.

    Parameters
    ----------
    webdriver : `webdriver.Chrome`
        The selenium webdriver.
    """
    webdriver.get(HOME)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_test_list(webdriver):
    """Get a list of all the lab tests on the website.

    Parameters
    ----------
    webdriver : `webdriver.Chrome`
        The selenium webdriver.

    Returns
    -------
    test : `list` of `unit_harvest.units_lab.Test`
        The lab test bage to navigate to.
    """
    # This finds the section to the file downloads
    item_list = webdriver.find_elements_by_xpath(
        '//div[@class="view-content"]/div[@class="item-list"]/ul/li//span/a'
    )
    return [Test(i.text, i.get_attribute('href'), i) for i in item_list]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def goto_test(webdriver, test):
    """Navigate to the test conversion page.

    Parameters
    ----------
    webdriver : `selanium.Webdriver`
        The webdriver to use.
    test : `unit_harvest.units_lab.Test`
        The lab test bage to navigate to.

    Returns
    -------
    test_region : `selanium.WebElement`
        The HTML region containing the lab test elements.
    input_region : `selanium.WebElement`
        The HTML region that contains the value input regions.
    """
    webdriver.get(test.url)
    test_region = webdriver.find_element_by_xpath(
        '//div[@role="main"]//div[@class="region region-content"]'
    )
    input_region = test_region.find_element_by_xpath(
        '//div[contains(@class, "pmd-card")]'
    )
    return test_region, input_region


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def input_units(webdriver, input_region, value):
    """Input the units in to the UI.

    Parameters
    ----------
    webdriver : `selanium.Webdriver`
        The webdriver to use.
    input_region : `selanium.WebElement`
        The HTML region that contains the value input regions.
    value : `int`
        The input value to test.

    Returns
    -------
    input_units : `str`
        The input units of the test value.
    """
    input_ok = False
    input_units = ""
    for i in range(1, 50):
        try:
            input_box = input_region.find_element_by_xpath(
                f'//div[@id="d{i}"]//div[@class="col-sm-9"]/'
                f'input[@name="mass{i}"]'
            )
            input_box.click()
            # clear(), click(), sendKeys()
            input_box.clear()
            input_box.click()
            input_box.send_keys(f"{value}")
            input_ok = True
            label_box = input_region.find_element_by_xpath(
                f'//div[@id="d{i}"]//label/span[@id="unit{i}"]'
            )
            input_units = label_box.text
            break
        except Exception:
            # print(e.args[0])
            # ElementNotInteractableException
            pass
    if input_ok is False:
        raise ValueError("can't input for test")
    webdriver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    input_region.find_element_by_xpath('//button[text()="Calculate"]').click()
    return input_units


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_conversion_units(webdriver, units):
    """Input the units in to the UI.

    Parameters
    ----------
    webdriver : `selanium.Webdriver`
        The webdriver to use.
    units : `list` of `int`
        The different unit types that have been detected.

    Returns
    -------
    conversions : `list` of (`str`, `str`)
        The conversions, each tuple is the units and the converted value.
    """
    n = len(units) + 10
    conversions = []
    for i in range(1, n):
        try:
            label_box = webdriver.find_element_by_xpath(
                f'//div[@id="d{i}"]//label/span[@id="unit{i}"]'
            )
            unit_label = label_box.text
            value_box = webdriver.find_element_by_xpath(
                f'//div[@id="d{i}"]//div[@class="col-sm-9"]/'
                f'input[@name="mass{i}"]'
            )
            value_box.click()
            unit_value = value_box.get_attribute('value')
            conversions.append((unit_label, unit_value))
        except Exception:
            pass
    return conversions


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_test_data(webdriver, test, unit_ranges=[1, 10, 25, 50, 75, 100]):
    """Get all the test data for for a clinical test. This is the synonyms,
    units used and the unit conversions.

    Parameters
    ----------
    webdriver : `selanium.Webdriver`
        The webdriver to use.
    test : `unit_harvest.units_lab.Test`
        The lab test bage to navigate to.
    unit_ranges : `list` of `int`
        Units to convert from. A range is being taken just in case I
        need to create a conversion formula.

    Returns
    -------
    test_data : `dict`
        A dict with the keys being units, synonyms, conversions.
    """
    # Will hold all the conversions
    conversions = dict()

    # Navigate to the test page
    test_region, input_region = goto_test(webdriver, test)

    # Get the synonyms and all the units available
    synonyms = get_synonyms(webdriver)
    units = get_units_of_measure_summary(webdriver)

    # Loop through a range of possible units
    for i in unit_ranges:
        input_units(webdriver, input_region, i)
        conversions[i] = get_conversion_units(webdriver, units)
    return dict(units=units, synonyms=synonyms, conversions=conversions)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_synonyms(webdriver):
    """Get the name synonyms for the lab test.

    Parameters
    ----------
    webdriver : `selanium.Webdriver`
        The webdriver to use.

    Returns
    -------
    synonyms : `list` of `str`
        The synonyms for the lab tests, if there are none then the list will be
        empty.
    """
    try:
        return _get_text_summary(webdriver, "Synonym")
    except NoSuchElementException:
        return []


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_units_of_measure_summary(webdriver):
    """Get the units of measure summary.

    Parameters
    ----------
    webdriver : `selanium.Webdriver`
        The webdriver to use.

    Returns
    -------
    units_of_measure : `list` of `str`
        The units of measure for the lab test.
    """
    return _get_text_summary(webdriver, "Units of measurement")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_text_summary(webdriver, title):
    """Extract text from an element that is under title.

    Parameters
    ----------
    webdriver : `selanium.Webdriver`
        The webdriver to use.
    title : `str`
        The name of the lab test.

    Returns
    -------
    text : `str`
        The text summary for the region.
    """
    text_element = webdriver.find_element_by_xpath(
        '//div[text() = "{0}"]/..'.format(title)
    )

    text_fields = []
    for i in re.split(r',\s+', text_element.text):
        i = re.sub('\n', '', i)
        i = re.sub(title, '', i).strip().strip(',')
        if i not in ['']:
            text_fields.append(i)
    return text_fields


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
