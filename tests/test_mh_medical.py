"""Tests to make sure the scrape still works for units lab. This uses web
 queries.
"""
from unit_harvest import mhmedical, common
from unit_harvest.example_data import examples
import pytest
import os
import gzip
import urllib.request
# import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def is_up(url):
    """Test to see if a webpage can be accessed.

    Parameters
    ----------
    url : `str`
        The URL to test.

    Returns
    -------
    is_up : `bool`
        True if a 200 code was received, False if not.
    """
    code = urllib.request.urlopen(url).getcode()
    return code == 200


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.dependency()
def test_internet():
    """Test that we can connect to the outside world
    """
    assert is_up('https://www.google.com/'), "resource not available"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.dependency(depends=['test_internet'])
def test_webdriver(tmpdir):
    """Test that the webdriver can be initialised, the webdriver needs to be
    in your path.
    """
    # Initialise the chrome webdriver
    webdriver = common.initialise_chrome(
        download_dir=str(tmpdir),
        headless=True
    )
    webdriver.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.dependency(depends=['test_internet'])
def test_mh_medical_is_up(tmpdir):
    """Test we can connect to MH medical
    """
    try:
        webdriver = common.initialise_chrome(
            download_dir=str(tmpdir),
            headless=True
        )
        mhmedical.go_home(webdriver)
    finally:
        webdriver.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.dependency(depends=['test_mh_medical_is_up'])
def test_download(tmpdir):
    """Test we can download MH medical and is it the same as we expect
    """
    exp_file = examples.get_data("mhmedical_path")
    outfile = os.path.join(str(tmpdir), "test_file.txt.gz")
    try:
        webdriver = common.initialise_chrome(
            download_dir=str(tmpdir),
            headless=True
        )
        mhmedical.go_home(webdriver)
        mhmedical.get_table_content(webdriver, outfile, verbose=False)
    finally:
        webdriver.close()

    e = gzip.open(exp_file, 'rt')
    o = gzip.open(outfile, 'rt')
    try:
        for el, ol in zip(e, o):
            assert el == ol, "wrong line"
    finally:
        e.close()
        o.close()
        os.unlink(outfile)
